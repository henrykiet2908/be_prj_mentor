﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mentor_v1.Application.Common.Mappings;
using mentor_v1.Domain.Enums;

namespace mentor_v1.Application.BlogCategory.Queries.GetCategory;

// Define mapping profile
public class CategoryViewModel : IMapFrom<Domain.Entities.BlogCategory>
{
    public Guid Id { get; set; }
    public string CategoryName { get; set; }
    public string CategoryDescrible { get; set; }
    public string Image { get; set; }
    public string Layout { get; set; }
    public string DisplayOrder { get; set; }
    public BlogCategoriesStatus Status { get; set; }
}
