﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using mentor_v1.Application.Common.Interfaces;
using mentor_v1.Application.Common.Models;
using Microsoft.EntityFrameworkCore;

namespace mentor_v1.Application.BlogCategory.Queries.GetCategory;

// IRequest<return type>
public class GetCategoryRequest : IRequest<PaginatedList<CategoryViewModel>>
{
    public int Page { get; set; }
    public int Size { get; set; }
}

// IRequestHandler<request type, return type>
public class GetCategoryRequestHandler : IRequestHandler<GetCategoryRequest, PaginatedList<CategoryViewModel>>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    // DI
    public GetCategoryRequestHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<PaginatedList<CategoryViewModel>> Handle(GetCategoryRequest request, CancellationToken cancellationToken)
    {
        // get categories
        var categories = _context.Get<Domain.Entities.BlogCategory>().Where(x=>x.IsDeleted==false).AsNoTracking();

        // map IQueryable<BlogCategory> to IQueryable<CategoryViewModel>
        // AsNoTracking to remove default tracking on entity framework
        var map = _mapper.ProjectTo<CategoryViewModel>(categories);

        // Paginate data
        var page = await PaginatedList<CategoryViewModel>
            .CreateAsync(map, request.Page, request.Size);

        return page;
    }
}
