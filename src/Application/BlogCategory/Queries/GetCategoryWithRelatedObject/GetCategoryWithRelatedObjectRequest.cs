﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using mentor_v1.Application.Common.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace mentor_v1.Application.BlogCategory.Queries.GetCategoryWithRelatedObject;

public class GetCategoryWithRelatedObjectRequest:IRequest<List<Domain.Entities.BlogCategory>>
{
    public int Page { get; set; }
    public int Size { get; set; }
}

public class GetCategoryWithRelatedObjectRequestHandler : IRequestHandler<GetCategoryWithRelatedObjectRequest, List<Domain.Entities.BlogCategory>>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetCategoryWithRelatedObjectRequestHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public Task<List<Domain.Entities.BlogCategory>> Handle(GetCategoryWithRelatedObjectRequest request, CancellationToken cancellationToken)
    {
        return _context.Get<Domain.Entities.BlogCategory>()
                .Include(x=>x.Blogs)
                // chain query
                //.Where(x => x.IsDeleted == false)
                .AsNoTracking()
                .ToListAsync(cancellationToken: cancellationToken);
    }
}
