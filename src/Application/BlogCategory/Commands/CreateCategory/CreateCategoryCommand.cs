﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using mentor_v1.Application.Common.Interfaces;
using mentor_v1.Domain.Enums;
using Microsoft.EntityFrameworkCore;

namespace mentor_v1.Application.BlogCategory.Commands.CreateCategory;

// Request from Mediatr 
public class CreateCategoryCommand : IRequest<Guid>
{
    public string CategoryName { get; set; }
    public string CategoryDescrible { get; set; }
    public string Image { get; set; }
    public string Layout { get; set; }
    public string DisplayOrder { get; set; }
    public BlogCategoriesStatus Status { get; set; }
}

// Handler to handle the request (Can be written to another file)
// CreateCategoryCommand : IRequest<Guid> => IRequestHandler<CreateCategoryCommand, Guid>
public class CreateCategoryCommandHandler : IRequestHandler<CreateCategoryCommand, Guid>
{
    private readonly IApplicationDbContext _context;

    public CreateCategoryCommandHandler(IApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<Guid> Handle(CreateCategoryCommand request, CancellationToken cancellationToken)
    {
        // create new category from request data
        var category = new Domain.Entities.BlogCategory() {
            CategoryName = request.CategoryName,
            CategoryDescrible = request.CategoryDescrible,
            Image = request.Image,
            Layout = request.Layout,
            DisplayOrder = request.DisplayOrder,
            Status = request.Status,
        };

        // add new category
        _context.Get<Domain.Entities.BlogCategory>().Add(category);

        // commit change to database
        // because the function is async so we await it
        await _context.SaveChangesAsync(cancellationToken);

        // return the Guid
        return category.Id;
    }
}
