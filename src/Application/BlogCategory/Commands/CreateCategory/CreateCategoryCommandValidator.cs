﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;
using mentor_v1.Application.Common.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace mentor_v1.Application.BlogCategory.Commands.CreateCategory;
public class CreateCategoryCommandValidator: AbstractValidator<CreateCategoryCommand>
{
    private readonly IApplicationDbContext _context;

    public CreateCategoryCommandValidator(IApplicationDbContext context)
    {
        _context = context;

        // Add validation for request
        RuleFor(v => v.CategoryName)
            .NotEmpty().WithMessage("Category name is required.")
            .MaximumLength(200).WithMessage("Category name must not exceed 200 characters.")
            // Can continue with multi chain
            //.MustAsync(BeUniqueName).WithMessage("The specified category name already exists.")
            //.MustAsync(BeUniqueName).WithMessage("The specified category name already exists.")
            .MustAsync(BeUniqueName).WithMessage("The specified category name already exists.");
    }

    // Custom action to check with the database
    public async Task<bool> BeUniqueName(string name, CancellationToken cancellationToken)
    {
        return await _context.Get<Domain.Entities.BlogCategory>()
            .AllAsync(l => l.CategoryName != name, cancellationToken);
    }
}
