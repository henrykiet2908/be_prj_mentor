﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using mentor_v1.Application.Common.Interfaces;
using mentor_v1.Domain.Entities;
using mentor_v1.Domain.Enums;

namespace mentor_v1.Application.City.Commands.CreateCity;

public class CreateCityCommand : IRequest<Guid>
{
    public string NameCity { get; set; }
    public string Description { get; set; }
    public int Code { get; set; }
    public IList<mentor_v1.Domain.Entities.District> Districts { get; set; }

}

public class CreateCityCommandHandler : IRequestHandler<CreateCityCommand, Guid>
{
    private readonly IApplicationDbContext _context;

    public CreateCityCommandHandler(IApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<Guid> Handle(CreateCityCommand request, CancellationToken cancellationToken)
    {
        var city = new Domain.Entities.City()
        {
            NameCity = request.NameCity,
            Description = request.Description,
            Code = request.Code
        };

        // add new category
        _context.Get<Domain.Entities.City>().Add(city);

        // commit change to database
        // because the function is async so we await it
        await _context.SaveChangesAsync(cancellationToken);

        // return the Guid
        return city.Id;
    }
}