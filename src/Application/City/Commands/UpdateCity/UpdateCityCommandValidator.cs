﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;
using mentor_v1.Application.Common.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace mentor_v1.Application.City.Commands.UpdateCity;

public class UpdateCityCommandValidator : AbstractValidator<UpdateCityCommand>
{
    private readonly IApplicationDbContext _context;

    public UpdateCityCommandValidator(IApplicationDbContext context)
    {
        _context = context;

        // Add validation for request
        RuleFor(v => v.NameCity)
            .NotEmpty().WithMessage("City name is required.")
            .MaximumLength(200).WithMessage("City name must not exceed 200 characters.")
            // Can continue with multi chain
            //.MustAsync(BeUniqueName).WithMessage("The specified City name already exists.")
            //.MustAsync(BeUniqueName).WithMessage("The specified City name already exists.")
            .MustAsync(BeUniqueCityName).WithMessage("The specified City name already exists.");

        RuleFor(v => v.Code)
           // Can continue with multi chain
           //.MustAsync(BeUniqueName).WithMessage("The specified Code already exists.")
           //.MustAsync(BeUniqueName).WithMessage("The specified Code already exists.")
           .MustAsync(BeUniqueCode).WithMessage("The specified Code already exists.");
    }

    // Custom action to check with the database
    public async Task<bool> BeUniqueCityName(string name, CancellationToken cancellationToken)
    {
        return await _context.Get<Domain.Entities.City>()
            .AllAsync(l => l.NameCity != name, cancellationToken);
    }
    public async Task<bool> BeUniqueCode(int Code, CancellationToken cancellationToken)
    {
        return await _context.Get<Domain.Entities.City>()
            .AllAsync(l => l.Code != Code, cancellationToken);
    }
}
