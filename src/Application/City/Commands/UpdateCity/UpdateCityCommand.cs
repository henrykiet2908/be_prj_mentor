﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using mentor_v1.Application.Common.Exceptions;
using mentor_v1.Application.Common.Interfaces;
using mentor_v1.Domain.Entities;

namespace mentor_v1.Application.City.Commands.UpdateCity;

public record UpdateCityCommand : IRequest
{
    public Guid Id { get; init; }
    public string NameCity { get; set; }
    public string Description { get; set; }
    public int Code { get; set; }
}

public class UpdateCityCommandHandler : IRequestHandler<UpdateCityCommand>
{
    private readonly IApplicationDbContext _context;

    public UpdateCityCommandHandler(IApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<Unit> Handle(UpdateCityCommand request, CancellationToken cancellationToken)
    {
        var CurrentCity = await _context.Get<Domain.Entities.City>()
            .FindAsync(new object[] { request.Id }, cancellationToken);

        if (CurrentCity == null)
        {
            throw new NotFoundException(nameof(Domain.Entities.City), request.Id);
        }
        CurrentCity.NameCity = request.NameCity;
        CurrentCity.Description= request.Description;
        CurrentCity.Code= request.Code;

        await _context.SaveChangesAsync(cancellationToken);

        return Unit.Value;
    }
}
