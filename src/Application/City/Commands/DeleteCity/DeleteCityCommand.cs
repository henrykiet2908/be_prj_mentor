﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using mentor_v1.Application.Common.Exceptions;
using mentor_v1.Application.Common.Interfaces;

namespace mentor_v1.Application.City.Commands.DeleteCity;

public record DeleteCityCommand : IRequest<bool>
{
    public Guid Id { get; init; }
}

public class DeleteCityCommandHandler : IRequestHandler<DeleteCityCommand,bool>
{
    private readonly IApplicationDbContext _context;

    public DeleteCityCommandHandler(IApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<bool> Handle(DeleteCityCommand request, CancellationToken cancellationToken)
    {
        var CurrentCity = await _context.Get<Domain.Entities.City>()
            .FindAsync(new object[] { request.Id }, cancellationToken);

        if (CurrentCity == null)
        {
            throw new NotFoundException(nameof(Domain.Entities.City), request.Id);
        }
        CurrentCity.IsDeleted = true;

        await _context.SaveChangesAsync(cancellationToken);

        return true;
    }
}
