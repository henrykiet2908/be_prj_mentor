﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
//using mentor_v1.Application.City.Queries.GetCityById;
using mentor_v1.Application.Common.Exceptions;
using mentor_v1.Application.Common.Interfaces;
using mentor_v1.Application.Common.Models;
using Microsoft.EntityFrameworkCore;

namespace mentor_v1.Application.City.Queries.GetCityByIdByRelatedObject;

public class GetCityByIdRequest : IRequest<GetCity.CityViewModel>
{
    public Guid Id { get; set; }
    
}

// IRequestHandler<request type, return type>
public class GetCityByIdRequestHandler : IRequestHandler<GetCityByIdRequest, GetCity.CityViewModel>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    // DI
    public GetCityByIdRequestHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public Task<GetCity.CityViewModel> Handle(GetCityByIdRequest request, CancellationToken cancellationToken)
    {
        // get categories
        var city = _context.Get<Domain.Entities.City>().Where(x => x.IsDeleted == false && x.Id.Equals(request.Id)).AsNoTracking().FirstOrDefault();
        if (city == null)
        {
            throw new NotFoundException(nameof(Domain.Entities.City), request.Id);
        }
        // map IQueryable<BlogCity> to IQueryable<GetCity.CityViewModel>
        // AsNoTracking to remove default tracking on entity framework
        var map = _mapper.Map<GetCity.CityViewModel>(city);

        // Paginate data
        return Task.FromResult(map); //Task.CompletedTask;
    }
}

