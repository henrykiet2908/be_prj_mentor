﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
//using mentor_v1.Application.BlogCity.Queries.GetCity;
using mentor_v1.Application.Common.Interfaces;
using mentor_v1.Application.Common.Models;
using Microsoft.EntityFrameworkCore;

namespace mentor_v1.Application.City.Queries.GetCity;
public class GetCityRequest : IRequest<PaginatedList<CityViewModel>>
{
    public int Page { get; set; }
    public int Size { get; set; }
}

// IRequestHandler<request type, return type>
public class GetCityRequestHandler : IRequestHandler<GetCityRequest, PaginatedList<CityViewModel>>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    // DI
    public GetCityRequestHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<PaginatedList<CityViewModel>> Handle(GetCityRequest request, CancellationToken cancellationToken)
    {
        // get categories
        var ListCity = _context.Get<Domain.Entities.City>().Where(x => x.IsDeleted == false).AsNoTracking();

        // map IQueryable<BlogCity> to IQueryable<CityViewModel>
        // AsNoTracking to remove default tracking on entity framework
        var map = _mapper.ProjectTo<CityViewModel>(ListCity);

        // Paginate data
        var page = await PaginatedList<CityViewModel>
            .CreateAsync(map, request.Page, request.Size);

        return page;
    }
}
