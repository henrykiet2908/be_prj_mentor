﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mentor_v1.Application.Common.Mappings;
using mentor_v1.Domain.Entities;
using mentor_v1.Domain.Enums;

namespace mentor_v1.Application.City.Queries.GetCity;

public class CityViewModel : IMapFrom<Domain.Entities.City>
{
    public Guid Id { get; set; }
    public string NameCity { get; set; }
    public string Description { get; set; }
    public int Code { get; set; }

    public IList<mentor_v1.Domain.Entities.District> Districts { get; set; } = new List<mentor_v1.Domain.Entities.District>();
}
