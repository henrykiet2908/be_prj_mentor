﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;
//using mentor_v1.Application.District.Commands.CreateDistrict;
using mentor_v1.Application.Common.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace mentor_v1.Application.District.Commands.CreateDistrict;

public class CreateDistrictCommandValidator : AbstractValidator<CreateDistrictCommand>
{
    private readonly IApplicationDbContext _context;

    public CreateDistrictCommandValidator(IApplicationDbContext context)
    {
        _context = context;

        // Add validation for request
        RuleFor(v => v.NameDistrict)
            .NotEmpty().WithMessage("District name is required.")
            .MaximumLength(200).WithMessage("District name must not exceed 200 characters.")
            // Can continue with multi chain
            //.MustAsync(BeUniqueName).WithMessage("The specified District name already exists.")
            //.MustAsync(BeUniqueName).WithMessage("The specified District name already exists.")
            .MustAsync(BeUniqueDistrictName).WithMessage("The specified District name already exists.");

        RuleFor(v => v.Code)
           // Can continue with multi chain
           //.MustAsync(BeUniqueName).WithMessage("The specified Code already exists.")
           //.MustAsync(BeUniqueName).WithMessage("The specified Code already exists.")
           .MustAsync(BeUniqueCode).WithMessage("The specified Code already exists.");
        RuleFor(v => v.CityId)
            .MustAsync(BeUniqueCityId).WithMessage("The specified Id City already exists.");
    }

    // Custom action to check with the database
    public async Task<bool> BeUniqueDistrictName(string name, CancellationToken cancellationToken)
    {
        return await _context.Get<Domain.Entities.District>()
            .AllAsync(l => l.NameDistrict != name, cancellationToken);
    }
    public async Task<bool> BeUniqueCode(int Code, CancellationToken cancellationToken)
    {
        return await _context.Get<Domain.Entities.District>()
            .AllAsync(l => l.Code != Code, cancellationToken);
    }
    public async Task<bool> BeUniqueCityId(Guid CityId, CancellationToken cancellationToken)
    {
        return await _context.Get<Domain.Entities.District>()
            .AllAsync(l => l.CityId != CityId, cancellationToken);
    }
}