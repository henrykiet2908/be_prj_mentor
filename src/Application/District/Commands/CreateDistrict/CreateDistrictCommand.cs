﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using mentor_v1.Application.Common.Interfaces;
using mentor_v1.Domain.Entities;

namespace mentor_v1.Application.District.Commands.CreateDistrict;

public class CreateDistrictCommand : IRequest<Guid>
{
    public Guid CityId { get; set; }
    public string NameDistrict { get; set; }
    public string Description { get; set; }
    public int Code { get; set; }
}

public class CreateDistrictCommandHandler : IRequestHandler<CreateDistrictCommand, Guid>
{
    private readonly IApplicationDbContext _context;

    public CreateDistrictCommandHandler(IApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<Guid> Handle(CreateDistrictCommand request, CancellationToken cancellationToken)
    {
        var District = new Domain.Entities.District()
        {
            NameDistrict = request.NameDistrict,
            Description = request.Description,
            Code = request.Code,
            CityId = request.CityId,
        };

        // add new category
        _context.Get<Domain.Entities.District>().Add(District);

        // commit change to database
        // because the function is async so we await it
        await _context.SaveChangesAsync(cancellationToken);

        // return the Guid
        return District.Id;
    }
}