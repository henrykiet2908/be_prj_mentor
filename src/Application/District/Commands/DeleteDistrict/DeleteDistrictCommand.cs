﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using mentor_v1.Application.Common.Exceptions;
using mentor_v1.Application.Common.Interfaces;

namespace mentor_v1.Application.District.Commands.DeleteDistrict;

public record DeleteDistrictCommand : IRequest<bool>
{
    public Guid Id { get; init; }
}

public class DeleteDistrictCommandHandler : IRequestHandler<DeleteDistrictCommand, bool>
{
    private readonly IApplicationDbContext _context;

    public DeleteDistrictCommandHandler(IApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<bool> Handle(DeleteDistrictCommand request, CancellationToken cancellationToken)
    {
        var CurrentDistrict = await _context.Get<Domain.Entities.District>()
            .FindAsync(new object[] { request.Id }, cancellationToken);

        if (CurrentDistrict == null)
        {
            throw new NotFoundException(nameof(Domain.Entities.District), request.Id);
        }
        CurrentDistrict.IsDeleted = true;

        await _context.SaveChangesAsync(cancellationToken);

        return true;
    }
}