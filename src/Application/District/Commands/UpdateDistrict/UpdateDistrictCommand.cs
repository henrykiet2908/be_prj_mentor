﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using mentor_v1.Application.Common.Exceptions;
using mentor_v1.Application.Common.Interfaces;

namespace mentor_v1.Application.District.Commands.UpdateDistrict;

public record UpdateDistrictCommand : IRequest
{
    public Guid Id { get; init; }
    public Guid CityId { get; set; }
    public string NameDistrict { get; set; }
    public string Description { get; set; }
    public int Code { get; set; }
}

public class UpdateDistrictCommandHandler : IRequestHandler<UpdateDistrictCommand>
{
    private readonly IApplicationDbContext _context;

    public UpdateDistrictCommandHandler(IApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<Unit> Handle(UpdateDistrictCommand request, CancellationToken cancellationToken)
    {
        var CurrentDistrict = await _context.Get<Domain.Entities.District>()
            .FindAsync(new object[] { request.Id }, cancellationToken);

        if (CurrentDistrict == null)
        {
            throw new NotFoundException(nameof(Domain.Entities.District), request.Id);
        }
        CurrentDistrict.NameDistrict = request.NameDistrict;
        CurrentDistrict.Description = request.Description;
        CurrentDistrict.Code = request.Code;
        CurrentDistrict.CityId = request.CityId;

        await _context.SaveChangesAsync(cancellationToken);

        return Unit.Value;
    }
}