﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mentor_v1.Application.Common.Mappings;

namespace mentor_v1.Application.District.Queries.GetDistrict;

public class DistrictViewModel : IMapFrom<Domain.Entities.District>
{
    public Guid DistrictId { get; set; }
    public Guid CityId { get; set; }
    public string CityName { get; set; }
    public string NameDistrict { get; set; }
    public string Description { get; set; }
    public int Code { get; set; }
    
    public IList<mentor_v1.Domain.Entities.Ward> Ward { get; set; } = new List<mentor_v1.Domain.Entities.Ward>();

}