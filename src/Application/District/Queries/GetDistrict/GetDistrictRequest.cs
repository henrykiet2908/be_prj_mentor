﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using mentor_v1.Application.District.Queries.GetDistrict;
using mentor_v1.Application.Common.Interfaces;
using mentor_v1.Application.Common.Models;
using mentor_v1.Application.District.Queries.GetDistrictByRelatedObject;
using Microsoft.EntityFrameworkCore;
//using mentor_v1.Application.District.Queries.GetDistrict;

namespace mentor_v1.Application.District.Queries.GetDistrict;

public class GetDistrictRequest : IRequest<PaginatedList<DistrictViewModel>>
{
    public int Page { get; set; }
    public int Size { get; set; }
}

// IRequestHandler<request type, return type>
public class GetDistrictRequestHandler : IRequestHandler<GetDistrictRequest, PaginatedList<DistrictViewModel>>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    // DI
    public GetDistrictRequestHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<PaginatedList<DistrictViewModel>> Handle(GetDistrictRequest request, CancellationToken cancellationToken)
    {
        // get categories
        var ListDistrict = _context.Get<Domain.Entities.District>().Where(x => x.IsDeleted == false).AsNoTracking();

        // map IQueryable<BlogDistrict> to IQueryable<DistrictViewModel>
        // AsNoTracking to remove default tracking on entity framework
        var map = _mapper.ProjectTo<DistrictViewModel>(ListDistrict);

        // Paginate data
        var page = await PaginatedList<DistrictViewModel>
            .CreateAsync(map, request.Page, request.Size);

        return page;
    }
}