﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using mentor_v1.Application.Common.Exceptions;
using mentor_v1.Application.Common.Interfaces;
using mentor_v1.Application.District.Queries.GetDistrict;
using mentor_v1.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace mentor_v1.Application.District.Queries.GetDistrictByRelatedObject;


public class GetDistrictByIdRequest : IRequest<DistrictViewModel>
{
    public Guid Id { get; set; }

}

// IRequestHandler<request type, return type>
public class GetDistrictByIdRequestHandler : IRequestHandler<GetDistrictByIdRequest, DistrictViewModel>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    // DI
    public GetDistrictByIdRequestHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public Task<DistrictViewModel> Handle(GetDistrictByIdRequest request, CancellationToken cancellationToken)
    {
        // get categories
        var District = _context.Get<Domain.Entities.District>()
                            .Where(x => x.IsDeleted == false && x.Id.Equals(request.Id))
                            .AsNoTracking().FirstOrDefault();
        if (District == null)
        {
            throw new NotFoundException(nameof(Domain.Entities.District), request.Id);
        }
        // map IQueryable<BlogDistrict> to IQueryable<GetDistrict.DistrictViewModel>
        // AsNoTracking to remove default tracking on entity framework
        var map = _mapper.Map<DistrictViewModel>(District);

        map.CityName = District.City.NameCity;

        // Paginate data
        return Task.FromResult(map); //Task.CompletedTask;
    }
}
