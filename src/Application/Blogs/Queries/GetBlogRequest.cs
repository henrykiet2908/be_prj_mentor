﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using mentor_v1.Application.Common.Interfaces;
using mentor_v1.Application.Common.Models;
using mentor_v1.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace mentor_v1.Application.Blogs.Queries;

public class GetBlogRequest:IRequest<PaginatedList<BlogViewModel>>
{
    public int Page { get; set; }
    public int Size { get; set; }
}

public class GetBlogRequestHandler : IRequestHandler<GetBlogRequest, PaginatedList<BlogViewModel>>
{
    private readonly IApplicationDbContext _applicationDbContext;
    private readonly IMapper _mapper;

    public GetBlogRequestHandler(IApplicationDbContext applicationDbContext, IMapper mapper)
    {
        _applicationDbContext = applicationDbContext;
        _mapper = mapper;
    }

    public Task<PaginatedList<BlogViewModel>> Handle(GetBlogRequest request, CancellationToken cancellationToken)
    {
        var blogs = _applicationDbContext.Get<Blog>().AsNoTracking();

        var models = _mapper.ProjectTo<BlogViewModel>(blogs);

        var page = PaginatedList<BlogViewModel>.CreateAsync(models, request.Page, request.Size);

        return page;
    }
}
