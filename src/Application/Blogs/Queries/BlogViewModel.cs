﻿using mentor_v1.Application.Common.Mappings;
using mentor_v1.Domain.Entities;
using mentor_v1.Domain.Enums;

namespace mentor_v1.Application.Blogs.Queries;

public class BlogViewModel:IMapFrom<Blog>
{
    public Guid BlogCategoryId { get; set; }

    public string Title { get; set; }
    public string Title_English { get; set; }
    public string Describle { get; set; }
    public string Describle_English { get; set; }
    public string Content { get; set; }
    public string Content_English { get; set; }
    public string Cover_Image { get; set; }
    public string Subsection_Image { get; set; }
    public BlogCategoriesStatus BCtatus { get; set; }
}