﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using mentor_v1.Application.Common.Exceptions;
using mentor_v1.Application.Common.Interfaces;
using mentor_v1.Application.CourseCourseImageMappingMapping.Queries.GetCourseImageMapping;
using mentor_v1.Application.CourseImageMapping.Queries.GetCourseImageMapping;
using Microsoft.EntityFrameworkCore;

namespace mentor_v1.Application.CourseImageMapping.Queries.GetCourseImageMappingByRelatedObject;

public class GetCourseImageMappingByIdRequest : IRequest<CourseImageMappingViewModel>
{
    public Guid Id { get; set; }

}

// IRequestHandler<request type, return type>
public class GetCourseImageMappingByIdRequestHandler : IRequestHandler<GetCourseImageMappingByIdRequest, CourseImageMappingViewModel>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    // DI
    public GetCourseImageMappingByIdRequestHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public Task<CourseImageMappingViewModel> Handle(GetCourseImageMappingByIdRequest request, CancellationToken cancellationToken)
    {
        // get categories
        var CourseImageMapping = _context.Get<Domain.Entities.CourseImageMapping>()
                            .Where(x => x.IsDeleted == false && x.Id.Equals(request.Id))
                            .AsNoTracking().FirstOrDefault();
        if (CourseImageMapping == null)
        {
            throw new NotFoundException(nameof(Domain.Entities.CourseImageMapping), request.Id);
        }
        // map IQueryable<BlogCourseImageMapping> to IQueryable<GetCourseImageMapping.CourseImageMappingViewModel>
        // AsNoTracking to remove default tracking on entity framework
        var map = _mapper.Map<CourseImageMappingViewModel>(CourseImageMapping);

        // Paginate data
        return Task.FromResult(map); //Task.CompletedTask;
    }
}