﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mentor_v1.Application.Common.Mappings;
using mentor_v1.Domain.Enums;

namespace mentor_v1.Application.CourseCourseImageMappingMapping.Queries.GetCourseImageMapping;

public class CourseImageMappingViewModel : IMapFrom<Domain.Entities.CourseImageMapping>
{
    public Guid CourseImageMappingId { get; set; }
    public Guid CourseId { get; set; }

    public string Theme { get; set; }
    public DefaultStatus Status { get; set; }

    public IList<mentor_v1.Domain.Entities.Image> Images { get; private set; } = new List<mentor_v1.Domain.Entities.Image>();
}