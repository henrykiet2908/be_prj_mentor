﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using mentor_v1.Application.Common.Interfaces;
using mentor_v1.Application.Common.Models;
using mentor_v1.Application.CourseCourseImageMappingMapping.Queries.GetCourseImageMapping;
using mentor_v1.Application.CourseImageMapping.Queries.GetCourseImageMapping;
using Microsoft.EntityFrameworkCore;

namespace mentor_v1.Application.CourseImageMapping.Queries.GetCourseImageMapping;

public class GetCourseImageMappingRequest : IRequest<PaginatedList<CourseImageMappingViewModel>>
{
    public int Page { get; set; }
    public int Size { get; set; }
}

// IRequestHandler<request type, return type>
public class GetCourseImageMappingRequestHandler : IRequestHandler<GetCourseImageMappingRequest, PaginatedList<CourseImageMappingViewModel>>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    // DI
    public GetCourseImageMappingRequestHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<PaginatedList<CourseImageMappingViewModel>> Handle(GetCourseImageMappingRequest request, CancellationToken cancellationToken)
    {
        // get categories
        var ListCourseImageMapping = _context.Get<Domain.Entities.CourseImageMapping>().Where(x => x.IsDeleted == false).AsNoTracking();

        // map IQueryable<BlogCourseImageMapping> to IQueryable<CourseImageMappingViewModel>
        // AsNoTracking to remove default tracking on entity framework
        var map = _mapper.ProjectTo<CourseImageMappingViewModel>(ListCourseImageMapping);

        // Paginate data
        var page = await PaginatedList<CourseImageMappingViewModel>
            .CreateAsync(map, request.Page, request.Size);

        return page;
    }
}