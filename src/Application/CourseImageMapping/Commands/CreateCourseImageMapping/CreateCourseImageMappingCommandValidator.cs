﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;
using mentor_v1.Application.Common.Interfaces;
using mentor_v1.Application.CourseImageMapping.Commands.CreateCourseImageMapping;
using Microsoft.EntityFrameworkCore;

namespace mentor_v1.Application.CourseCourseImageMappingMapping.Commands.CreateCourseCourseImageMappingMapping;

public class CreateCourseImageMappingCommandValidator : AbstractValidator<CreateCourseImageMappingCommand>
{
    private readonly IApplicationDbContext _context;

    public CreateCourseImageMappingCommandValidator(IApplicationDbContext context)
    {
        _context = context;

        // Add validation for request
        RuleFor(v => v.Theme)
            .NotEmpty().WithMessage("Theme is required.")
            .MaximumLength(200).WithMessage("Theme must not exceed 200 characters.")
            // Can continue with multi chain
            //.MustAsync(BeUniqueTheme).WithMessage("The specified Theme already exists.")
            //.MustAsync(BeUniqueTheme).WithMessage("The specified Theme already exists.")
            .MustAsync(BeUniqueCourseImageMappingTheme).WithMessage("The specified Theme already exists.");
        RuleFor(v => v.CourseId)
            .MustAsync(BeUniqueCourseId).WithMessage("The specified Id City already exists.");
    }

    // Custom action to check with the database
    public async Task<bool> BeUniqueCourseImageMappingTheme(string Theme, CancellationToken cancellationToken)
    {
        return await _context.Get<mentor_v1.Domain.Entities.CourseImageMapping>()
            .AllAsync(l => l.Theme != Theme, cancellationToken);
    }
    public async Task<bool> BeUniqueCourseId(Guid CourseId, CancellationToken cancellationToken)
    {
        return await _context.Get<Domain.Entities.CourseImageMapping>()
            .AllAsync(l => l.CourseId != CourseId, cancellationToken);
    }
}