﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using mentor_v1.Application.Common.Interfaces;
using mentor_v1.Domain.Enums;

namespace mentor_v1.Application.CourseImageMapping.Commands.CreateCourseImageMapping;

public class CreateCourseImageMappingCommand : IRequest<Guid>
{
    public Guid CourseId { get; set; }

    public string Theme { get; set; }
    public DefaultStatus Status { get; set; }


}

public class CreateCourseImageMappingCommandHandler : IRequestHandler<CreateCourseImageMappingCommand, Guid>
{
    private readonly IApplicationDbContext _context;

    public CreateCourseImageMappingCommandHandler(IApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<Guid> Handle(CreateCourseImageMappingCommand request, CancellationToken cancellationToken)
    {
        var CourseImageMapping = new Domain.Entities.CourseImageMapping()
        {
            CourseId = request.CourseId,
            Theme = request.Theme,
            Status = request.Status,
        };

        // add new category
        _context.Get<Domain.Entities.CourseImageMapping>().Add(CourseImageMapping);

        // commit change to database
        // because the function is async so we await it
        await _context.SaveChangesAsync(cancellationToken);

        // return the Guid
        return CourseImageMapping.Id;
    }
}