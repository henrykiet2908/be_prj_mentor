﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using mentor_v1.Application.Common.Exceptions;
using mentor_v1.Application.Common.Interfaces;
using mentor_v1.Domain.Enums;

namespace mentor_v1.Application.CourseImageMapping.Commands.UpdateCourseImageMapping;

public record UpdateCourseImageMappingCommand : IRequest
{
    public Guid Id { get; init; }
    public Guid CourseId { get; set; }

    public string Theme { get; set; }
    public DefaultStatus Status { get; set; }

}

public class UpdateCourseImageMappingCommandHandler : IRequestHandler<UpdateCourseImageMappingCommand>
{
    private readonly IApplicationDbContext _context;

    public UpdateCourseImageMappingCommandHandler(IApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<Unit> Handle(UpdateCourseImageMappingCommand request, CancellationToken cancellationToken)
    {
        var CurrentCourseImageMapping = await _context.Get<Domain.Entities.CourseImageMapping>()
            .FindAsync(new object[] { request.Id }, cancellationToken);

        if (CurrentCourseImageMapping == null)
        {
            throw new NotFoundException(nameof(Domain.Entities.CourseImageMapping), request.Id);
        }
        CurrentCourseImageMapping.CourseId = request.CourseId;
        CurrentCourseImageMapping.Theme = request.Theme;
        CurrentCourseImageMapping.Status = request.Status;

        await _context.SaveChangesAsync(cancellationToken);

        return Unit.Value;
    }
}