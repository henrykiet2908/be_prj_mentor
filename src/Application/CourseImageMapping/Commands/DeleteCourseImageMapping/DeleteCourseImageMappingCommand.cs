﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using mentor_v1.Application.Common.Exceptions;
using mentor_v1.Application.Common.Interfaces;

namespace mentor_v1.Application.CourseImageMapping.Commands.DeleteCourseImageMapping;

public record DeleteCourseImageMappingCommand : IRequest<bool>
{
    public Guid Id { get; init; }
}

public class DeleteCourseImageMappingCommandHandler : IRequestHandler<DeleteCourseImageMappingCommand, bool>
{
    private readonly IApplicationDbContext _context;

    public DeleteCourseImageMappingCommandHandler(IApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<bool> Handle(DeleteCourseImageMappingCommand request, CancellationToken cancellationToken)
    {
        var CurrentCourseImageMapping = await _context.Get<Domain.Entities.CourseImageMapping>()
            .FindAsync(new object[] { request.Id }, cancellationToken);

        if (CurrentCourseImageMapping == null)
        {
            throw new NotFoundException(nameof(Domain.Entities.CourseImageMapping), request.Id);
        }
        CurrentCourseImageMapping.IsDeleted = true;

        await _context.SaveChangesAsync(cancellationToken);

        return true;
    }
}