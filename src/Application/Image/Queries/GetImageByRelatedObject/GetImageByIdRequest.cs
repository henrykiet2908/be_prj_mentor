﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using mentor_v1.Application.Common.Exceptions;
using mentor_v1.Application.Common.Interfaces;
using mentor_v1.Application.Image.Queries.GetImage;
using Microsoft.EntityFrameworkCore;

namespace mentor_v1.Application.Image.Queries.GetImageByIdRequest;

public class GetImageByIdRequest : IRequest<ImageViewModel>
{
    public Guid Id { get; set; }

}

// IRequestHandler<request type, return type>
public class GetImageByIdRequestHandler : IRequestHandler<GetImageByIdRequest, ImageViewModel>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    // DI
    public GetImageByIdRequestHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public Task<ImageViewModel> Handle(GetImageByIdRequest request, CancellationToken cancellationToken)
    {
        // get categories
        var Image = _context.Get<Domain.Entities.Image>()
                            .Where(x => x.IsDeleted == false && x.Id.Equals(request.Id))
                            .AsNoTracking().FirstOrDefault();
        if (Image == null)
        {
            throw new NotFoundException(nameof(Domain.Entities.Image), request.Id);
        }
        // map IQueryable<BlogImage> to IQueryable<GetImage.ImageViewModel>
        // AsNoTracking to remove default tracking on entity framework
        var map = _mapper.Map<ImageViewModel>(Image);

        // Paginate data
        return Task.FromResult(map); //Task.CompletedTask;
    }
}