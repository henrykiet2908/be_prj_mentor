﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mentor_v1.Application.Common.Mappings;
using mentor_v1.Domain.Enums;

namespace mentor_v1.Application.Image.Queries.GetImage;

public class ImageViewModel : IMapFrom<Domain.Entities.Image>
{
    public Guid ImageId { get; set; }
    public Guid CourseImageMappingId { get; set; }

    public string Type { get; set; }
    public string Note { get; set; }
    public DefaultStatus Status { get; set; }
}