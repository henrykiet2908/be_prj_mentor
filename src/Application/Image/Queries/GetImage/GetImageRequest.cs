﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using mentor_v1.Application.Common.Interfaces;
using mentor_v1.Application.Common.Models;
using mentor_v1.Application.Image.Queries.GetImage;
using Microsoft.EntityFrameworkCore;

namespace mentor_v1.Application.Image.Queries.GetImage;

public class GetImageRequest : IRequest<PaginatedList<ImageViewModel>>
{
    public int Page { get; set; }
    public int Size { get; set; }
}

// IRequestHandler<request type, return type>
public class GetImageRequestHandler : IRequestHandler<GetImageRequest, PaginatedList<ImageViewModel>>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    // DI
    public GetImageRequestHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<PaginatedList<ImageViewModel>> Handle(GetImageRequest request, CancellationToken cancellationToken)
    {
        // get categories
        var ListImage = _context.Get<Domain.Entities.Image>().Where(x => x.IsDeleted == false).AsNoTracking();

        // map IQueryable<BlogImage> to IQueryable<ImageViewModel>
        // AsNoTracking to remove default tracking on entity framework
        var map = _mapper.ProjectTo<ImageViewModel>(ListImage);

        // Paginate data
        var page = await PaginatedList<ImageViewModel>
            .CreateAsync(map, request.Page, request.Size);

        return page;
    }
}