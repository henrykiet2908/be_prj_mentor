﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using mentor_v1.Application.Common.Exceptions;
using mentor_v1.Application.Common.Interfaces;
using mentor_v1.Domain.Enums;

namespace mentor_v1.Application.Image.Commands.UpdateImage;

public record UpdateImageCommand : IRequest
{
    public Guid Id { get; init; }
    public Guid CourseImageMappingId { get; set; }
    public string Type { get; set; }
    public string Note { get; set; }
    public DefaultStatus Status { get; set; }
}

public class UpdateImageCommandHandler : IRequestHandler<UpdateImageCommand>
{
    private readonly IApplicationDbContext _context;

    public UpdateImageCommandHandler(IApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<Unit> Handle(UpdateImageCommand request, CancellationToken cancellationToken)
    {
        var CurrentImage = await _context.Get<Domain.Entities.Image>()
            .FindAsync(new object[] { request.Id }, cancellationToken);

        if (CurrentImage == null)
        {
            throw new NotFoundException(nameof(Domain.Entities.Image), request.Id);
        }
        CurrentImage.CourseImageMappingId = request.CourseImageMappingId;
        CurrentImage.Type = request.Type;
        CurrentImage.Note = request.Note;
        CurrentImage.Status = request.Status;

        await _context.SaveChangesAsync(cancellationToken);

        return Unit.Value;
    }
}