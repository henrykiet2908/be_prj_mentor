﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;
using mentor_v1.Application.Common.Interfaces;
using mentor_v1.Application.Image.Commands.CreateImage;
using Microsoft.EntityFrameworkCore;

//Typespace mentor_v1.Application.Image.Commands.CreateImage;

public class CreateImageCommandValidator : AbstractValidator<CreateImageCommand>
{
    private readonly IApplicationDbContext _context;

    public CreateImageCommandValidator(IApplicationDbContext context)
    {
        _context = context;

        // Add validation for request
        RuleFor(v => v.Type)
            .NotEmpty().WithMessage("Type is required.")
            .MaximumLength(200).WithMessage("Type must not exceed 200 characters.")
            // Can continue with multi chain
            //.MustAsync(BeUniqueType).WithMessage("The specified Type already exists.")
            //.MustAsync(BeUniqueType).WithMessage("The specified Type already exists.")
            .MustAsync(BeUniqueImageType).WithMessage("The specified Type already exists.");

        RuleFor(v => v.Note)
            .MaximumLength(200).WithMessage("The Note must not exceed 200 characters");
        RuleFor(v => v.CourseImageMappingId)
            .MustAsync(BeUniqueCourseImageMappingId).WithMessage("The specified Id City already exists.");
    }

    // Custom action to check with the database
    public async Task<bool> BeUniqueImageType(string Type, CancellationToken cancellationToken)
    {
        return await _context.Get<mentor_v1.Domain.Entities.Image>()
            .AllAsync(l => l.Type != Type, cancellationToken);
    }
    public async Task<bool> BeUniqueCourseImageMappingId(Guid CourseImageMappingId, CancellationToken cancellationToken)
    {
        return await _context.Get<mentor_v1.Domain.Entities.Image>()
            .AllAsync(l => l.CourseImageMappingId != CourseImageMappingId, cancellationToken);
    }
}
