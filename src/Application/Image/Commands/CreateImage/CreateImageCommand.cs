﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using mentor_v1.Application.Common.Interfaces;
using mentor_v1.Domain.Enums;

namespace mentor_v1.Application.Image.Commands.CreateImage;

public class CreateImageCommand : IRequest<Guid>
{
    public Guid CourseImageMappingId { get; set; }
    public string Type { get; set; }
    public string Note { get; set; }
    public DefaultStatus Status { get; set; }

}

public class CreateImageCommandHandler : IRequestHandler<CreateImageCommand, Guid>
{
    private readonly IApplicationDbContext _context;

    public CreateImageCommandHandler(IApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<Guid> Handle(CreateImageCommand request, CancellationToken cancellationToken)
    {
        var Image = new Domain.Entities.Image()
        {
            CourseImageMappingId = request.CourseImageMappingId,
            Type = request.Type,
            Note = request.Note,
            Status = request.Status,
        };

        // add new category
        _context.Get<Domain.Entities.Image>().Add(Image);

        // commit change to database
        // because the function is async so we await it
        await _context.SaveChangesAsync(cancellationToken);

        // return the Guid
        return Image.Id;
    }
}
