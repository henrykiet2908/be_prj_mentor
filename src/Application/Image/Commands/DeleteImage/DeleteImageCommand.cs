﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using mentor_v1.Application.Common.Exceptions;
using mentor_v1.Application.Common.Interfaces;

namespace mentor_v1.Application.Image.Commands.DeleteImage;

public record DeleteImageCommand : IRequest<bool>
{
    public Guid Id { get; init; }
}

public class DeleteImageCommandHandler : IRequestHandler<DeleteImageCommand, bool>
{
    private readonly IApplicationDbContext _context;

    public DeleteImageCommandHandler(IApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<bool> Handle(DeleteImageCommand request, CancellationToken cancellationToken)
    {
        var CurrentImage = await _context.Get<Domain.Entities.Image>()
            .FindAsync(new object[] { request.Id }, cancellationToken);

        if (CurrentImage == null)
        {
            throw new NotFoundException(nameof(Domain.Entities.Image), request.Id);
        }
        CurrentImage.IsDeleted = true;

        await _context.SaveChangesAsync(cancellationToken);

        return true;
    }
}