﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using mentor_v1.Application.Common.Exceptions;
using mentor_v1.Application.Common.Interfaces;
using mentor_v1.Application.Ward.Queries.GetWard;
using Microsoft.EntityFrameworkCore;

namespace mentor_v1.Application.Ward.Queries.GetWardByRelatedObject;

public class GetWardByIdRequest : IRequest<WardViewModel>
{
    public Guid Id { get; set; }

}

// IRequestHandler<request type, return type>
public class GetWardByIdRequestHandler : IRequestHandler<GetWardByIdRequest, WardViewModel>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    // DI
    public GetWardByIdRequestHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public Task<WardViewModel> Handle(GetWardByIdRequest request, CancellationToken cancellationToken)
    {
        // get categories
        var Ward = _context.Get<Domain.Entities.Ward>()
                            .Where(x => x.IsDeleted == false && x.Id.Equals(request.Id))
                            .AsNoTracking().FirstOrDefault();
        if (Ward == null)
        {
            throw new NotFoundException(nameof(Domain.Entities.Ward), request.Id);
        }
        // map IQueryable<BlogWard> to IQueryable<GetWard.WardViewModel>
        // AsNoTracking to remove default tracking on entity framework
        var map = _mapper.Map<WardViewModel>(Ward);


        // Paginate data
        return Task.FromResult(map); //Task.CompletedTask;
    }
}