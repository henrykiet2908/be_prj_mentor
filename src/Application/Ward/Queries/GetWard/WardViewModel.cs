﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mentor_v1.Application.Common.Mappings;

namespace mentor_v1.Application.Ward.Queries.GetWard;

public class WardViewModel : IMapFrom<Domain.Entities.Ward>
{
    public Guid WardId { get; set; }
    public Guid DistrictId { get; set; }
    public string NameWard { get; set; }
    public string Description { get; set; }
    public int Code { get; set; }

    public IList<mentor_v1.Domain.Entities.Course> Course { get; set; } = new List<mentor_v1.Domain.Entities.Course>();

}