﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using mentor_v1.Application.Common.Interfaces;
using mentor_v1.Application.Common.Models;
using mentor_v1.Application.Ward.Queries.GetWard;
using Microsoft.EntityFrameworkCore;

namespace mentor_v1.Application.Ward.Queries.GetWard;

public class GetWardRequest : IRequest<PaginatedList<WardViewModel>>
{
    public int Page { get; set; }
    public int Size { get; set; }
}

// IRequestHandler<request type, return type>
public class GetWardRequestHandler : IRequestHandler<GetWardRequest, PaginatedList<WardViewModel>>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    // DI
    public GetWardRequestHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<PaginatedList<WardViewModel>> Handle(GetWardRequest request, CancellationToken cancellationToken)
    {
        // get categories
        var ListWard = _context.Get<Domain.Entities.Ward>().Where(x => x.IsDeleted == false).AsNoTracking();

        // map IQueryable<BlogWard> to IQueryable<WardViewModel>
        // AsNoTracking to remove default tracking on entity framework
        var map = _mapper.ProjectTo<WardViewModel>(ListWard);

        // Paginate data
        var page = await PaginatedList<WardViewModel>
            .CreateAsync(map, request.Page, request.Size);

        return page;
    }
}