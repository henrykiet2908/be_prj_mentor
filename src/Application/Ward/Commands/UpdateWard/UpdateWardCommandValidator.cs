﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;
using mentor_v1.Application.Common.Interfaces;
using mentor_v1.Application.Ward.Commands.UpdateWard;
using Microsoft.EntityFrameworkCore;

namespace mentor_v1.Application.Ward.Commands.UpdateWard;

public class UpdateWardCommandValidator : AbstractValidator<UpdateWardCommand>
{
    private readonly IApplicationDbContext _context;

    public UpdateWardCommandValidator(IApplicationDbContext context)
    {
        _context = context;

        // Add validation for request
        RuleFor(v => v.NameWard)
            .NotEmpty().WithMessage("Ward name is required.")
            .MaximumLength(200).WithMessage("Ward name must not exceed 200 characters.")
            // Can continue with multi chain
            //.MustAsync(BeUniqueName).WithMessage("The specified Ward name already exists.")
            //.MustAsync(BeUniqueName).WithMessage("The specified Ward name already exists.")
            .MustAsync(BeUniqueWardName).WithMessage("The specified Ward name already exists.");

        RuleFor(v => v.Code)
           // Can continue with multi chain
           //.MustAsync(BeUniqueName).WithMessage("The specified Code already exists.")
           //.MustAsync(BeUniqueName).WithMessage("The specified Code already exists.")
           .MustAsync(BeUniqueCode).WithMessage("The specified Code already exists.");
        RuleFor(v => v.DistrictId)
           .MustAsync(BeUniqueDistrictId).WithMessage("The specified Id District already exists.");
    }

    // Custom action to check with the database
    public async Task<bool> BeUniqueWardName(string name, CancellationToken cancellationToken)
    {
        return await _context.Get<Domain.Entities.Ward>()
            .AllAsync(l => l.NameWard != name, cancellationToken);
    }
    public async Task<bool> BeUniqueCode(int Code, CancellationToken cancellationToken)
    {
        return await _context.Get<Domain.Entities.Ward>()
            .AllAsync(l => l.Code != Code, cancellationToken);
    }
    public async Task<bool> BeUniqueDistrictId(Guid DistrictId, CancellationToken cancellationToken)
    {
        return await _context.Get<mentor_v1.Domain.Entities.Ward>()
            .AllAsync(l => l.DistrictId != DistrictId, cancellationToken);
    }
}