﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using mentor_v1.Application.Common.Exceptions;
using mentor_v1.Application.Common.Interfaces;

namespace mentor_v1.Application.Ward.Commands.UpdateWard;

public record UpdateWardCommand : IRequest
{
    public Guid Id { get; init; }
    public Guid DistrictId { get; set; }
    public string NameWard { get; set; }
    public string Description { get; set; }
    public int Code { get; set; }
}

public class UpdateWardCommandHandler : IRequestHandler<UpdateWardCommand>
{
    private readonly IApplicationDbContext _context;

    public UpdateWardCommandHandler(IApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<Unit> Handle(UpdateWardCommand request, CancellationToken cancellationToken)
    {
        var CurrentWard = await _context.Get<Domain.Entities.Ward>()
            .FindAsync(new object[] { request.Id }, cancellationToken);

        if (CurrentWard == null)
        {
            throw new NotFoundException(nameof(Domain.Entities.Ward), request.Id);
        }
        CurrentWard.NameWard = request.NameWard;
        CurrentWard.Description = request.Description;
        CurrentWard.Code = request.Code;
        CurrentWard.DistrictId = request.DistrictId;

        await _context.SaveChangesAsync(cancellationToken);

        return Unit.Value;
    }
}