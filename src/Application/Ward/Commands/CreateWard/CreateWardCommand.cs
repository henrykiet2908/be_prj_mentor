﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using mentor_v1.Application.Common.Interfaces;
using mentor_v1.Domain.Entities;

namespace mentor_v1.Application.Ward.Commands.CreateWard;

public class CreateWardCommand : IRequest<Guid>
{
    public Guid DistrictId { get; set; }
    public string NameWard { get; set; }
    public string Description { get; set; }
    public int Code { get; set; }

}

public class CreateWardCommandHandler : IRequestHandler<CreateWardCommand, Guid>
{
    private readonly IApplicationDbContext _context;

    public CreateWardCommandHandler(IApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<Guid> Handle(CreateWardCommand request, CancellationToken cancellationToken)
    {
        var Ward = new Domain.Entities.Ward()
        {
            NameWard = request.NameWard,
            Description = request.Description,
            Code = request.Code,
            DistrictId = request.DistrictId,
        };

        // add new category
        _context.Get<Domain.Entities.Ward>().Add(Ward);

        // commit change to database
        // because the function is async so we await it
        await _context.SaveChangesAsync(cancellationToken);

        // return the Guid
        return Ward.Id;
    }
}
