﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using mentor_v1.Application.Common.Exceptions;
using mentor_v1.Application.Common.Interfaces;

namespace mentor_v1.Application.Ward.Commands.DeleteWard;

public record DeleteWardCommand : IRequest<bool>
{
    public Guid Id { get; init; }
}

public class DeleteWardCommandHandler : IRequestHandler<DeleteWardCommand, bool>
{
    private readonly IApplicationDbContext _context;

    public DeleteWardCommandHandler(IApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<bool> Handle(DeleteWardCommand request, CancellationToken cancellationToken)
    {
        var CurrentWard = await _context.Get<Domain.Entities.Ward>()
            .FindAsync(new object[] { request.Id }, cancellationToken);

        if (CurrentWard == null)
        {
            throw new NotFoundException(nameof(Domain.Entities.Ward), request.Id);
        }
        CurrentWard.IsDeleted = true;

        await _context.SaveChangesAsync(cancellationToken);

        return true;
    }
}