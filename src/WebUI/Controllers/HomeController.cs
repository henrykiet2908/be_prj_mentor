﻿using mentor_v1.Application.Blogs.Queries;
using mentor_v1.WebUI.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace WebUI.Controllers;
public class HomeController : ApiControllerBase
{
    [HttpGet("/blogs")]
    public async Task<IActionResult> Index()
    {
        var blogs = await Mediator.Send(new GetBlogRequest() { Page= 1,Size=10 });
        return Ok(blogs);
    }
}
