﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mentor_v1.Domain.Entities;

public class OrderDetail : BaseAuditableEntity
{
    [ForeignKey("Order")]
    public Guid OrderId { get; set; }

    public string IdOfCourse { get; set; }

    [ForeignKey("ClassCourse")]
    public Guid ClassCourseId { get; set; }
  
    public double Fee { get; set; }

    public virtual Order Order { get; set; }
    public virtual ClassCourse ClassCourse { get; set; }
}