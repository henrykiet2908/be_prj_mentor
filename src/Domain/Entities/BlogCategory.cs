﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mentor_v1.Domain.Entities;

public class BlogCategory : BaseAuditableEntity
{
    public string CategoryName { get; set; }
    public string CategoryDescrible { get; set; }
    public string Image { get; set; }
    public string Layout { get; set; }
    public string DisplayOrder { get; set; }
    public BlogCategoriesStatus Status { get; set; }

    public IList<Blog> Blogs { get; private set; } = new List<Blog>();
}