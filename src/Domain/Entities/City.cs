﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mentor_v1.Domain.Entities;

public class City : BaseAuditableEntity
{
    public string NameCity { get; set; }
    public string Description { get; set; }
    public int Code { get; set; }

    public IList<District> Districts { get; private set; } = new List<District>();
}