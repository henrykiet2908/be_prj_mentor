﻿using mentor_v1.Domain.Identity;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace mentor_v1.Domain.Entities;

public class Withdraw : BaseAuditableEntity
{
   
    [ForeignKey("ApplicationUser")]
    public string ApplicationUserId { get; set; }

    public double Amount { get; set; }
    public int TypeWithdraw { get; set; }
    public WithdrawStatus Status { get; set; }
    public double BeforWallet { get; set; }
    public double AfterWallet { get; set; }

    public virtual ApplicationUser ApplicationUser { get; set; } = new ApplicationUser();

}
