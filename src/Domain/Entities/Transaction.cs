﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mentor_v1.Domain.Identity;

namespace mentor_v1.Domain.Entities;

public class Transaction : BaseAuditableEntity
{
    [ForeignKey("Order")]
    public Guid OrderId { get; set; }
    
    public double Amount { get; set; }
    public string IpnURL { get; set; }
    public string OrderInfo { get; set; }
    public string PartnerCode { get; set; }
    public string RedirectUrl { get; set; }
    public Guid RequestId { get; set; }
    public string RequestType { get; set; }
    public TransactionStatus Status { get; set; }
    public string PaymentMethod { get; set; }

    public virtual Order Order { get; set; } = new Order();    
}

