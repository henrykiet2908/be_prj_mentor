﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mentor_v1.Domain.Identity;

namespace mentor_v1.Domain.Entities;

public class CustomerAccount : BaseAuditableEntity
{
    [ForeignKey("ApplicationUser")]
    public string ApplicationUserId { get; set; }
   
    public string School { get; set; }
    public string Career_Orientation { get; set; }
    public DefaultStatus CusStatus { get; set; }

    public IList<Order> Orders { get; private set; } = new List<Order>();
    
    public virtual ApplicationUser ApplicationUser { get; set; } = new ApplicationUser();
}