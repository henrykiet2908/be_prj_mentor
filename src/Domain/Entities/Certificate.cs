﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace mentor_v1.Domain.Entities;

public class Certificate : BaseAuditableEntity
{
   
    [ForeignKey("MentorAccount")]
    public Guid MentorAccountId { get; set; }
    
    public string CertificateName { get; set; }
    public string Description { get; set; }
    public string ImageUpload { get; set; }

    public virtual MentorAccount MentorAccount { get; set; } = new MentorAccount();

}
