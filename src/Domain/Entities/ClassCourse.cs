﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mentor_v1.Domain.Entities;

public class ClassCourse : BaseAuditableEntity
{
    [ForeignKey("Course")]
    public Guid CourseId { get; set; }
    public DateTime StartTime { get; set; }
    public DateTime EndTime { get; set; }
    public string CodeClass { get; set; }
    public string ErrorReason { get; set; }
    public int NumOfStudent { get; set; }
    public ClassCourseStatus Status { get; set; }

    public IList<OrderDetail> OrderDetails { get; private set; } = new List<OrderDetail>();
    public IList<Feedback> Feedbacks { get; private set; } = new List<Feedback>();
    
    public virtual Course Course { get; set; } = new Course();

}