﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mentor_v1.Domain.Entities;

public class CourseCategory : BaseAuditableEntity
{
    
    public string NameList { get; set; }
    public string Description { get; set; }
    public string Tags { get; set; }

    public IList<Course> Courses { get; private set; } = new List<Course>();
}