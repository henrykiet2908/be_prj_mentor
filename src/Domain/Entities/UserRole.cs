﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mentor_v1.Domain.Identity;

namespace mentor_v1.Domain.Entities;
public class UserRole : BaseAuditableEntity
{
 public string Name { get; set; }   
    public string Position { get; set; }

    public IList<ApplicationUser> ApplicationUsers { get; private set; } = new List<ApplicationUser>();
}
