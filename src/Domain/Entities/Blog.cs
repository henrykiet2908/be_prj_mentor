﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mentor_v1.Domain.Entities;

public class Blog : BaseAuditableEntity
{
    [ForeignKey("BlogCategory")]
    public Guid BlogCategoryId { get; set; }
    
    public string Title { get; set; }
    public string Title_English { get; set; }
    public string Describle { get; set; }
    public string Describle_English { get; set; }
    public string Content { get; set; }
    public string Content_English { get; set; }
    public string Cover_Image { get; set; }
    public string Subsection_Image { get; set; }
    public BlogCategoriesStatus BCtatus { get; set; }

    public virtual BlogCategory BlogCategory { get; set; } = new BlogCategory();
}
