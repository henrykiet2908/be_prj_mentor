﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mentor_v1.Domain.Entities;

public class Course : BaseAuditableEntity
{
    [ForeignKey("CourseCategory")]
    public Guid? CourseCategoryId { get; set; }
    [ForeignKey("MentorAccount")]
    public Guid? MentorAccountId { get; set; }
    [ForeignKey("Ward")]
    public Guid? WardId { get; set; }
   
    public string NameCourse { get; set; }
    //public DateTime StartTime { get; set; }
    //public DateTime EndTime { get; set; }
    public string TypeOfLearning { get; set; }
    public double Price { get; set; }
    public string Infomation_Course { get; set; }
    public double ReferenceDiscount { get; set; }
    public string Tags { get; set; }
    public DefaultStatus Status { get; set; }

    public IList<ClassCourse> ClassCourses { get; private set; } = new List<ClassCourse>();
    public IList<CourseImageMapping> CourseImageMappings { get; private set; } = new List<CourseImageMapping>();
    
    public virtual CourseCategory CourseCategory { get; set; } = new CourseCategory();
    public virtual MentorAccount MentorAccount { get; set; }= new MentorAccount();
    public virtual Ward Ward { get; set; } = new Ward();

}