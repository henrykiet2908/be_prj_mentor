﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mentor_v1.Domain.Entities;

public class CourseImageMapping : BaseAuditableEntity
{
    [ForeignKey("Course")]
    public Guid CourseId { get; set; }
    
    public string Theme { get; set; }
    public DefaultStatus Status { get; set; }

    public IList<Image> Images { get; private set; } = new List<Image>();

    public virtual Course Course { get; set; } = new Course();
}