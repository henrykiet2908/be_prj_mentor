﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mentor_v1.Domain.Entities;

public class Order : BaseAuditableEntity
{
    [ForeignKey("CustomerAccount")]
    public Guid CustomerAccountId { get; set; }
   
    public string ReferId { get; set; }
    public double CenterBonus { get; set; }
    public double StudentBonus { get; set; }
    public double ReferBonus { get; set; }
    public double CenterBonusPercent { get; set; }
    public double StudentBonusPercent { get; set; }
    public double ReferBonusPercent { get; set; }
    public double Total { get; set; }
    public double TotalBonus { get; set; }
    public OrderStatus Status { get; set; }

    public virtual IList<Transaction> Transactions { get; private set; } = new List<Transaction>();
    public virtual IList<OrderDetail> OrderDetails { get; private set; } = new List<OrderDetail>();

    public virtual CustomerAccount CustomerAccount { get; set; } = new CustomerAccount();
}