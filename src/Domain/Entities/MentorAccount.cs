﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mentor_v1.Domain.Identity;

namespace mentor_v1.Domain.Entities;

public class MentorAccount : BaseAuditableEntity
{
    [ForeignKey("ApplicationUser")]
    public string ApplicationUserId { get; set; }
    
    public string Introduce { get; set; }
    public int NumOfYearOfExperience { get; set; }
    public string Skill { get; set; }
    public DefaultStatus Status { get; set; }


    public IList<Certificate> Certificates { get; private set; } = new List<Certificate>();
    public IList<Course> Courses { get; private set; } = new List<Course>();

    public virtual ApplicationUser ApplicationUser { get; set; } = new ApplicationUser();
}