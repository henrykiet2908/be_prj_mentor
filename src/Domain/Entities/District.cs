﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mentor_v1.Domain.Entities;

public class District : BaseAuditableEntity
{
    
    [ForeignKey("City")]
    public Guid? CityId { get; set; }

    public string NameDistrict { get; set; }
    public string Description { get; set; }
    public int Code { get; set; }

    public IList<Ward> Wards { get; private set; } = new List<Ward>();

    public virtual City City { get; set; } = new City();
}