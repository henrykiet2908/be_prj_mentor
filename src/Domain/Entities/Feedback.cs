﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mentor_v1.Domain.Entities;

public class Feedback : BaseAuditableEntity
{ 
    public string CustomerAccount_Id { get; set; }
    [ForeignKey("ClassCourse")]
    public Guid ClassCourseId { get; set; }
    
    public int Rating { get; set; }
    public string Comment { get; set; }

    public virtual ClassCourse ClassCourse { get; set; } = new ClassCourse();
}