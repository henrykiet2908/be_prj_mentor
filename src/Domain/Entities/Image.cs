﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mentor_v1.Domain.Entities;

public class Image : BaseAuditableEntity
{
    [ForeignKey("CourseImageMapping")]
    public Guid CourseImageMappingId { get; set; }
  
    public string Type { get; set; }
    public string Note { get; set; }
    public DefaultStatus Status { get; set; }

    public virtual CourseImageMapping CourseImageMapping { get; set; } = new CourseImageMapping();
}