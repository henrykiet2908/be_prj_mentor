﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mentor_v1.Domain.Entities;
public class Ward : BaseAuditableEntity
{
    [ForeignKey("District")]
    public Guid? DistrictId { get; set; }
    
    public string NameWard { get; set; }
    public string Description { get; set; }
    public int Code { get; set; }

    public IList<Course> Courses { get; private set; } = new List<Course>();

    public virtual District District { get; set; } = new District();
}