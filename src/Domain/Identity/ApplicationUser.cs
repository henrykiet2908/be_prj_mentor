﻿using Microsoft.AspNetCore.Identity;

namespace mentor_v1.Domain.Identity;

public class ApplicationUser : IdentityUser
{
    public string? FullName { get; set; }
    public DateTime BirthDay { get; set; }
    public string? Address { get; set; }
    public string? City { get; set; }
    public string? Ward { get; set; }
    public string? District { get; set; }
    public string? Avatar { get; set; }
    public string? AvatarUrl { get; set; }
    public UserStatus Status { get; set; }
    public double WalletBalance { get; set; }

    public Guid? MentorAccountId { get; set; }
    public Guid? ManagerAccountId { get; set; }
    public Guid? CustomerAccountId { get; set; }

    public virtual MentorAccount MentorAccount { get; set; } = new MentorAccount();
    public virtual ManagerAccount ManagerAccount { get; set; } = new ManagerAccount();
    public virtual CustomerAccount CustomerAccount { get; set; }= new CustomerAccount();

    public IList<Transaction> Transaction { get; private set; } = new List<Transaction>();


}
