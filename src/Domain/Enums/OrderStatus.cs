﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mentor_v1.Domain.Enums;
public enum OrderStatus
{
    Pending = 0,
    Done = 1,
    Cancel = 2,
    Fail = 3,
}
