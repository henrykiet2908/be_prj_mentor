﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mentor_v1.Domain.Enums;

public enum WithdrawStatus
{
    New = 0,
    Done = 1,
    Cancel = 2,
}