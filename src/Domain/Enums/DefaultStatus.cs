﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mentor_v1.Domain.Enums;
public enum DefaultStatus
{
    Deactive = 0,
    Active = 1,
}
